Projeto : "Como a influência do meio no Ensino Médio podem contribuir na evasão no Ensino Superior no período de 2014 e 2021?"

Objetivos Específicos :
>Relacionar os efeitos das decisões dos estudantes na evasão dos cursos de graduação;

>Discutir as maiores taxas de evasão por campus ;

>Relatar o impacto econômico ocasionado pela evasão do Ensino Superior.
